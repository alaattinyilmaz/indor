package tr.com.netas.indornav.Views.IndoorMapView;

public interface OnMapSymbolListener {

    boolean onMapSymbolClick(BaseMapSymbol mapSymbol);

}
