package tr.com.netas.indornav.Constants;

public class MapConstants {
    public static final double X_OFFSET_PIXEL = 116;
    public static final double Y_OFFSET_PIXEL = 190;
    public static final double ONE_METER_PIXEL = 177;
    public static final float AZIMUTH_DEVIATION_ANGLE = 39;
}
