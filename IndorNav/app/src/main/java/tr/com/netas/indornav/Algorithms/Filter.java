package tr.com.netas.indornav.Algorithms;

import java.util.ArrayList;
import java.util.List;

import tr.com.netas.indornav.Models.RSSIEntry;

import static tr.com.netas.indornav.Constants.Devices.NUMBER_OF_ACCESS_POINTS;

public class Filter {

    public Filter(){}

    public double[] applyAverageFilter(ArrayList<RSSIEntry> testPoints)
    {
        double[] filteredData = new double[NUMBER_OF_ACCESS_POINTS];
        int numberOfRows = testPoints.size();
        int[] apCounts = new int[NUMBER_OF_ACCESS_POINTS];

        for (int i = 0; i < numberOfRows; i++)
        {
            for (int j = 0; j < NUMBER_OF_ACCESS_POINTS; j++)
            {
                if(testPoints.get(i).getDevice()==(j+1))
                {
                    filteredData[j] += testPoints.get(i).getRssi();
                    apCounts[j]++;
                    break;
                }
            }
        }

        for (int k = 0; k < NUMBER_OF_ACCESS_POINTS; k++)
        {
            if(apCounts[k] == 0)
            { filteredData[k] = -1; }
            else
                { filteredData[k] = filteredData[k]/apCounts[k]; }
        }

        return filteredData;
    }

    public void weightedSlidingWindow(ArrayList<RSSIEntry> testPoints){


    }

}