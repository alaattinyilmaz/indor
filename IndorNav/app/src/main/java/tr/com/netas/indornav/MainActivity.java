package tr.com.netas.indornav;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tr.com.netas.indornav.Algorithms.Filter;
import tr.com.netas.indornav.Algorithms.MatchingAlgorithm;
import tr.com.netas.indornav.Constants.Devices;
import tr.com.netas.indornav.Database.CSVReader;
import tr.com.netas.indornav.Models.RSSIEntry;
import tr.com.netas.indornav.Views.IndoorMapView.MapView;
import tr.com.netas.indornav.Views.IndoorMapView.OnRealLocationMoveListener;
import tr.com.netas.indornav.Views.IndoorMapView.Position;

import static tr.com.netas.indornav.Constants.MapConstants.AZIMUTH_DEVIATION_ANGLE;


public class MainActivity extends AppCompatActivity {

    private long startTime;
    private long endTime;
    private long elapsedMilliSeconds;
    private double elapsedSeconds;
    final static int REQUEST_ENABLE_BT = 1;

    BluetoothAdapter bluetoothAdapter;
    BluetoothManager bluetoothManager;
    BluetoothLeScanner bluetoothScanner;
    ArrayList scanFilters;
    ArrayList<RSSIEntry> rssiEntries;

    // These scan settings are used to scan desired devices (see: Contants.Devices)
    ScanSettings settings = new ScanSettings.Builder().setScanMode(ScanSettings.CALLBACK_TYPE_ALL_MATCHES).build();
    int[] apCounts = new int[Devices.NUMBER_OF_ACCESS_POINTS];
    RSSIEntry rssiEntry;
    private Filter testPointFilter = new Filter();
    private MatchingAlgorithm matchingAlgorithm;


    // Views
    private MapView mMapView;
    private TextView mInfoTextView;
    private ImageButton navigator;
    private TextView directions;

    // Compass members
    private Compass compass;
    private ImageView arrowView;
    private float currentAzimuth;
    private float theta;
    private boolean isLocating = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        // Initializing Indoor Floor Map views
        mMapView = (MapView) findViewById(R.id.mapview);
        mInfoTextView = (TextView) findViewById(R.id.tv_current_location);
        directions = (TextView) findViewById(R.id.directions);
        navigator = (ImageButton) findViewById(R.id.navigator);
        arrowView = (ImageView) findViewById(R.id.main_image_hands);

        // Initializing of the map
        try {
            mMapView.initNewMap(getAssets().open("floormap.png"), 1, 90, new Position(8,4,true));
        } catch (IOException e) {
            e.printStackTrace();
        }

        compass = new Compass(this);
        Compass.CompassListener cl = new Compass.CompassListener() {

            @Override
            public void onNewAzimuth(float azimuth) {
                adjustArrow(azimuth);
            }
        };
        compass.setListener(cl);

        // STARTING POINT: 116, 190 (0,0)
        // LAST POINT: 2857, 190
        // TOP LEFT: 116, 1164

        // X TO BOTTOM: 116 OFFSET
        // Y OFFSET: 190
        // 177 PIXEL = 1 M
        // 266 PIXEL = 1.5 M

        // Initial dummy current location of the map (red spot)
        double xco = 4;
        double yco = 4;

        Position pixelLocation = new Position(xco,yco,true);
        mMapView.updateMyLocation(new Position(pixelLocation.getX(), pixelLocation.getY()));

        // Bluetooth Configurations
        bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        bluetoothScanner = bluetoothAdapter.getBluetoothLeScanner();

        // Ensures Bluetooth is available on the device and it is enabled. If not, displays a dialog requesting user permission to enable Bluetooth.
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BT);
        }

        // Checks ACCESS_COARSE_LOCATION and ACCESS_FINE_LOCATION permissions
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
                Toast.makeText(this, "The permission to get BLE location data is required", Toast.LENGTH_SHORT).show();
            }else{
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }

        scanFilters = new ArrayList<>();

        // Adding the filtered devices to the device filters list of bluetoothscanner
        for (int i = 0; i< Devices.FILTERED_DEVICES.length ; i++) {
            ScanFilter filter = new ScanFilter.Builder().setDeviceName(Devices.FILTERED_DEVICES[i]).build();
            scanFilters.add(filter);
        }

        // Initializing Database
        List<double[]> dataBase = new ArrayList<>();
        CSVReader csvReader = new CSVReader(MainActivity.this, "rssi_database.csv");
        dataBase = csvReader.getDB();

        matchingAlgorithm = new MatchingAlgorithm();
        matchingAlgorithm.setDataBase(dataBase);

        mMapView.setOnRealLocationMoveListener(new OnRealLocationMoveListener() {
            @Override
            public void onMove(Position position) {
                mInfoTextView.setText(position.toString());
            }
        });

        changeText("Please place the marker to your destination.");

        navigator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigator.setPressed(true);
                mMapView.requestNewRoute();
                startScanning();
            }
        });

    }


    // This method is called when a new RSSI value is obtained from any of the access points
    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {

            // Rotating animation of the navigator image
            Animation an = new RotateAnimation(180, 360,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                    0.5f);
            an.setDuration(200);
            an.setRepeatCount(0);
            an.setFillAfter(true);
            navigator.startAnimation(an);

            changeText("Locating your position...");

            // Collecting and filtering the data according to access points
            for (int i = 0; i < Devices.NUMBER_OF_ACCESS_POINTS; i++)
            {
                if(result.getDevice().getName().equals(Devices.FILTERED_DEVICES[i]))
                {
                    apCounts[i]++;
                    rssiEntry = new RSSIEntry();
                    rssiEntry.setRssi(result.getRssi());
                    rssiEntry.setDevice(i+1);
                    rssiEntries.add(rssiEntry);
                    System.out.println("Collecting DATA "+ result.getRssi());
                    break;
                }
            }

            endTime = SystemClock.elapsedRealtime();
            elapsedMilliSeconds = endTime - startTime;
            elapsedSeconds = elapsedMilliSeconds / 1000.0;
            isLocating = true;

            /*
            // It waits minimum 5 seconds
            if(elapsedSeconds >= 4 && elapsedSeconds <= 10)
            {
                updateCurrentMapLocation();
                isLocating = false;
                //stopScanning();
            }
            else if(elapsedSeconds >= 15)
            {
                // After 15 seconds collecting RSSI data is stopped it can be rescanned from navigator button
                updateCurrentMapLocation();
                isLocating = false;
                stopScanning();
            }
            */
            if(elapsedSeconds <= 15)
            {
                updateCurrentMapLocation();
                isLocating = false;
            }
            else {
                stopScanning();
                isLocating = false;
            }



        }
    };

    // Checks data is recieved from all access points it is not being used but I am leaving it here
    private boolean checkAllAccessPoints(){
        for (int i = 0; i < Devices.NUMBER_OF_ACCESS_POINTS; i++){
            if(apCounts[i]==0)
            { return false; }
        }
        return true;
    }

    public void startScanning() {
        startTime = SystemClock.elapsedRealtime();
        rssiEntries = new ArrayList<>();
        System.out.println("Start scanning");
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                bluetoothScanner.startScan(scanFilters, settings, leScanCallback);
            }
        });
    }

    // Stops the bluetooth scanning of RSSI values
    public void stopScanning() {
        navigator.setPressed(false);
        System.out.println("stopping scanning");
        Toast.makeText(MainActivity.this, "Bluetooth Scan is finished!", Toast.LENGTH_SHORT).show();
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                bluetoothScanner.stopScan(leScanCallback);
                }
        });
    }

    // This function adjust the compass according to your destination
    private void adjustArrow(float azimuth) {
        mMapView.getmMyLocationSymbol().setAzimuth(azimuth);
        theta = mMapView.getmMyLocationSymbol().getTheta();

        // You should also consider azimuth daviation angle that it deviation between the North of the map and the real North
        azimuth = -azimuth + theta + AZIMUTH_DEVIATION_ANGLE;
        Animation an = new RotateAnimation(currentAzimuth, azimuth,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        currentAzimuth = azimuth;
        an.setDuration(500);
        an.setRepeatCount(0);
        an.setFillAfter(true);
        arrowView.startAnimation(an);

        // Getting directions from routeFinder because it is vulnerable to the compass angle
        if(mMapView.getmMyLocationSymbol().isNewRouteRequest() && !isLocating)
        changeText(mMapView.getmMyLocationSymbol().getDirections());
    }


    // Compass actions according to application resume, start and pause events
    @Override
    protected void onStart() {
        super.onStart();
        compass.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        compass.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        compass.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        compass.stop();
    }

    // Changes text of directions area
    public void changeText(String text){
        directions.setText(text);
    }

    // This function estimates the position of the person
    public void updateCurrentMapLocation(){
        double[] testPoint = testPointFilter.applyAverageFilter(rssiEntries);
        int k = 4; // k of the weighted k-nearest-neighboor algorithm
        Position pos = matchingAlgorithm.PhyWeightedKNearestAlgorithm(testPoint,k); // Estimated position that is obtained from our algorithm
        mMapView.updateMyLocation(pos); // Updating the position and centering the view
        mMapView.centerMyLocation();
        mInfoTextView.setText(pos.toString());
    }

}
