package tr.com.netas.indornav;

import tr.com.netas.indornav.Views.IndoorMapView.Position;

import static tr.com.netas.indornav.Constants.MapConstants.AZIMUTH_DEVIATION_ANGLE;

public class RouteFinder {

    private Position markerLocation;
    private Position personLocation;
    private double xDif;
    private double yDif;
    private float azimuth;
    private String xDirection;
    private String yDirection;
    float theta;
    float directionAngle;
    double temp;

    public void setAzimuth(float azimuth) {
        this.azimuth = azimuth;
    }

    public float getTheta() {
        return theta;
    }

    public RouteFinder (){}

    public RouteFinder (Position personLocation, Position markerLocation){
        this.personLocation = personLocation;
        this.markerLocation = markerLocation;
        xDif = Math.round(Math.abs(markerLocation.getXm()-personLocation.getXmFromPixel())*100.0)/100.0;
        yDif = Math.round(Math.abs(markerLocation.getYm()-personLocation.getYmFromPixel())*100.0)/100.0;
    }

    public String getDirections(){

        theta = (float) Math.toDegrees(Math.atan(yDif/xDif));
        // BOTTOM LEFT
        if(markerLocation.getX() > personLocation.getX()  && markerLocation.getY() > personLocation.getY())
        {
            theta = theta + 180;
            xDirection = "bottom";
            yDirection = "left";
        }
        // BOTTOM RIGHT
        else if(markerLocation.getX() > personLocation.getX()  && markerLocation.getY() < personLocation.getY())
        { theta = theta + 90;
            xDirection = "bottom";
            yDirection = "right";
        }
        // TOP LEFT
        else if(markerLocation.getX() < personLocation.getX()  && markerLocation.getY() > personLocation.getY())
        { theta = theta + 270;
            xDirection = "top";
            yDirection = "left";
        }
        // TOP RIGHT
        else if(markerLocation.getX() < personLocation.getX() && markerLocation.getY() < personLocation.getY())
        { theta = theta;
            xDirection = "top";
            yDirection = "right";
        }

        directionAngle = -azimuth + theta + AZIMUTH_DEVIATION_ANGLE;
        while(directionAngle < 0)
        {
            directionAngle += 360;
            if(directionAngle >= 360)
                directionAngle = directionAngle % 360;
        }

        if(directionAngle >= 0 && directionAngle <= 90)
        {
            if((xDirection == "top" && yDirection == "left") || (xDirection== "bottom" && yDirection=="right"))
            {
                return ("Walk " + yDif + " meter to the top" + "\n Walk " + xDif + " meter to the right");
            }
            else
                return ("Walk " + yDif + " meter to the right" + "\n Walk " + xDif + " meter to the top");
        }
        else if(directionAngle >= 90 && directionAngle <= 180)
        {
            if((xDirection == "bottom" && yDirection == "left") || (xDirection== "top" && yDirection=="right"))
            {
                return ("Walk " + yDif + " meter to the bottom" + "\n Walk " + xDif + " meter to the right");
            }
            else
                return ("Walk " + yDif + " meter to the right" + "\n Walk " + xDif + " meter to the bottom");
        }
        else if(directionAngle >= 180 && directionAngle <= 270)
        {
            if((xDirection == "top" && yDirection == "left") || (xDirection== "bottom" && yDirection=="right"))
            {
                return ("Walk " + yDif + " meter to the bottom" + "\n Walk " + xDif + " meter to the left");
            }
            else
                return ("Walk " + yDif + " meter to the left" + "\n Walk " + xDif + " meter to the bottom");
        }
        else if(directionAngle >= 270 && directionAngle <= 360)
        {
            if((xDirection == "top" && yDirection == "right") || (xDirection== "bottom" && yDirection=="left"))
            {
                return ("Walk " + yDif + " meter to the top" + "\n Walk " + xDif + " meter to the left");
            }
            else
                return ("Walk " + yDif + " meter to the left" + "\n Walk " + xDif + " meter to the top");
        }
        else return "Locating your position...";
    }


}
