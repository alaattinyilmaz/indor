package tr.com.netas.indornav.Views.IndoorMapView;

/**
 * Created by moziliang on 16/6/27.
 */
public interface OnRealLocationMoveListener {
    void onMove(Position position);
}
