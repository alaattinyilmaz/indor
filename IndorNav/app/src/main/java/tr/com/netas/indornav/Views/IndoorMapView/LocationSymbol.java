package tr.com.netas.indornav.Views.IndoorMapView;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;

import tr.com.netas.indornav.RouteFinder;

public class LocationSymbol extends BaseMapSymbol {

    private float mRadius;
    private Paint mCirclePaint = null;
    private Paint mRangeCirclePaint = null;
    private Paint mCircleEdgePaint = null;
    private float mRangeRadius = 0;
    private int mRangeCircleColor = 0x00ffffff;
    private Paint pathPaint;
    private Path path = new Path();
    private RouteFinder routeFinder;
    private String directions = "Locating your position...";

    float theta;
    float azimuth;

    public void setAzimuth(float azimuth) {
        this.azimuth = azimuth;
    }

    public String getDirections() {
        if(routeFinder != null){
            this.directions = routeFinder.getDirections();
        }
        return directions;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }

    public RouteFinder getRouteFinder() {
        return routeFinder;
    }

    public float getTheta() {
        return theta;
    }

    private float[] markerLocationValues;
    private float[] pathMarkerLocationValues;

    private Position markerPosition;
    private Position personLocation;

    public Position getPersonLocation() {
        return personLocation;
    }

    public void setPersonLocation(Position personLocation) {
        this.personLocation = personLocation;
    }

    public void setMarkerPosition(Position markerPosition) {
        this.markerPosition = markerPosition;
    }

    private boolean newRouteRequest = false;

    public boolean isNewRouteRequest() {
        return newRouteRequest;
    }

    public void setNewRouteRequest(boolean newRouteRequest) {
        this.newRouteRequest = newRouteRequest;
    }

    public void setMarkerLocationValues(float[] markerLocationValues) {
        this.markerLocationValues = markerLocationValues;
    }
    public void setPathMarkerLocationValues(float[] pathMarkerLocationValues) {
        this.pathMarkerLocationValues = pathMarkerLocationValues;
    }

    public LocationSymbol(int mMainColor, int mEdgeColor, float mRadius) {
        this.mThreshold = 0f;
        this.mRotation = 0f;
        this.mVisibility = true;
        this.mOnMapSymbolListener = null;
        this.mRadius = mRadius;
        mCirclePaint = new Paint();
        mCirclePaint.setStyle(Style.FILL);
        mCirclePaint.setColor(mMainColor);
        mCirclePaint.setAntiAlias(true);
        mCircleEdgePaint = new Paint();
        mCircleEdgePaint.setStyle(Style.STROKE);
        mCircleEdgePaint.setColor(mEdgeColor);
        mCirclePaint.setAntiAlias(true);
    }

    public void setRangeCircle(float rangeCircleRadius, int rangeCircleColor) {
        mRangeRadius = rangeCircleRadius;
        mRangeCirclePaint = new Paint();
        mRangeCirclePaint.setStyle(Style.FILL);
        mRangeCircleColor = rangeCircleColor;
        mRangeCirclePaint.setColor(mRangeCircleColor);
    }

    @Override
    public void draw(Canvas canvas, Matrix matrix, float scale) {
        if (!mVisibility || scale < mThreshold)
            return;
        float[] locationValue = new float[]{(float) mLocation.getX(),
                (float) mLocation.getY()};
        matrix.mapPoints(locationValue);

        // paint range circle
        if (mRangeCirclePaint != null) {
            float radiusValue = mRangeRadius * scale;
            canvas.drawCircle(locationValue[0], locationValue[1], radiusValue, mRangeCirclePaint);
        }

        // paint circle
        canvas.drawCircle(locationValue[0], locationValue[1], mRadius, mCirclePaint);
        canvas.drawCircle(locationValue[0], locationValue[1], mRadius + 1, mCircleEdgePaint);

        pathPaint = new Paint();
        pathPaint.setColor(Color.BLUE);
        pathPaint.setStrokeWidth(25);
        pathPaint.setStyle(Paint.Style.STROKE);
        pathPaint.setAlpha(75);

        // PATH DRAWING
        if(newRouteRequest)
        {
            path = new Path();
            path.moveTo(locationValue[0], locationValue[1]);
            path.lineTo(pathMarkerLocationValues[0], pathMarkerLocationValues[1]);
            path.lineTo(markerLocationValues[0], markerLocationValues[1]);
            canvas.drawPath(path, pathPaint);
            routeFinder = new RouteFinder(personLocation,markerPosition);
            routeFinder.setAzimuth(azimuth);
            setDirections(routeFinder.getDirections());
            theta = routeFinder.getTheta();
        }

    }

    @Override
    public boolean isPointInClickRect(float x, float y) {
        return false;
    }
}
