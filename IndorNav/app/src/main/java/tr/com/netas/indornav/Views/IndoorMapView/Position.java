package tr.com.netas.indornav.Views.IndoorMapView;

import java.util.Locale;

import tr.com.netas.indornav.Constants.MapConstants;

/**
 * Created by limkuan on 15/6/10.
 */
public class Position {
    public Position(double x, double y) {
        this.x = x;
        this.y = y;
    }

    // Converting to Pixel location
    public Position(double x, double y, boolean pixel) {
        this.x = x*MapConstants.ONE_METER_PIXEL + MapConstants.X_OFFSET_PIXEL;
        this.y = y*MapConstants.ONE_METER_PIXEL + MapConstants.Y_OFFSET_PIXEL;
        this.xm = x;
        this.ym = y;
    }

    Position() {
    }

    public double getXmFromPixel(){
        this.xm = (x-MapConstants.X_OFFSET_PIXEL)/MapConstants.ONE_METER_PIXEL;
        return getXm();
    }

    public double getYmFromPixel(){
        this.ym = (y-MapConstants.Y_OFFSET_PIXEL)/MapConstants.ONE_METER_PIXEL;
        return getYm();
    }

    private double x;
    private double y;

    private double xm;
    private double ym;

    public double getXm() {
        return xm;
    }

    public void setXm(double xm) {
        this.xm = xm;
    }

    public double getYm() {
        return ym;
    }

    public void setYm(double ym) {
        this.ym = ym;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "x: %f, y: %f", getXmFromPixel(), getYmFromPixel());
    }

    public void convertToPixel()
    {

    }
}
