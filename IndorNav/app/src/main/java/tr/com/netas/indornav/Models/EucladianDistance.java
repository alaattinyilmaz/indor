package tr.com.netas.indornav.Models;

public class EucladianDistance {

    private int dbIndex;
    private double eucDist;
    private double refDist;
    private double weight;
    private double DDW;
    private double RPDW;


    public EucladianDistance(){}

    public double getDDW() {
        return DDW;
    }

    public void setDDW(double DDW) {
        this.DDW = DDW;
    }

    public double getRPDW() {
        return RPDW;
    }

    public void setRPDW(double RPDW) {
        this.RPDW = RPDW;
    }
    public EucladianDistance(int dbIndex, double eucDist){
        this.eucDist = eucDist;
        this.dbIndex = dbIndex;
    }

    public EucladianDistance(int dbIndex, double eucDist, double refDist){
        this.eucDist = eucDist;
        this.dbIndex = dbIndex;
        this.refDist = refDist;
    }

    public int getDbIndex() {
        return dbIndex;
    }

    public void setDbIndex(int dbIndex) {
        this.dbIndex = dbIndex;
    }

    public double getEucDist() {
        return eucDist;
    }

    public void setEucDist(double eucDist) {
        this.eucDist = eucDist;
    }

    public double getRefDist() {
        return refDist;
    }

    public void setRefDist(double refDist) {
        this.refDist = refDist;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }


}
