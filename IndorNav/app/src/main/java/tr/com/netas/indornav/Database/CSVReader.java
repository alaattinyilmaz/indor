package tr.com.netas.indornav.Database;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import tr.com.netas.indornav.Constants.Devices;

public class CSVReader {
    Context context;
    String fileName;
    List<String[]> rows;
    List<double[]> dataBase;

    public CSVReader(Context context, String fileName) {
        this.context = context;
        this.fileName = fileName;
        try {
            rows = readCSV();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String[]> readCSV() throws IOException {
        rows = new ArrayList<>();
        InputStream is = context.getAssets().open(fileName);
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line;
        String csvSplitBy = ",";
        br.readLine();
        while ((line = br.readLine()) != null) {
            String[] row = line.split(csvSplitBy);
            rows.add(row);
        }
        return rows;
    }

    public List<double[]> getDB() {
        try {
            rows = readCSV();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        dataBase = new ArrayList<>();
        for (int i = 0; i < rows.size();i++)
        {
            double[] dbRow = new double[Devices.NUMBER_OF_ACCESS_POINTS+2];
            for (int j = 0; j < Devices.NUMBER_OF_ACCESS_POINTS+2; j++)
            {
                dbRow[j] = Double.parseDouble(rows.get(i)[j]);
            }
            dataBase.add(dbRow);
        }
        return dataBase;
    }
}