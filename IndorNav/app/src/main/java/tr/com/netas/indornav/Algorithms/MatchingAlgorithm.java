package tr.com.netas.indornav.Algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import tr.com.netas.indornav.Models.EucladianDistance;
import tr.com.netas.indornav.Views.IndoorMapView.Position;

import static tr.com.netas.indornav.Constants.Devices.NUMBER_OF_ACCESS_POINTS;
import static tr.com.netas.indornav.Constants.Devices.TX_POWER;

public class MatchingAlgorithm {

    List<EucladianDistance> eucDistances;
    List <double[]> dataBase;

    public MatchingAlgorithm(){}

    public List<EucladianDistance> getEucDistances() {
        return eucDistances;
    }

    public void setEucDistances(List<EucladianDistance> eucDistances) {
        this.eucDistances = eucDistances;
    }

    public List<double[]> getDataBase() {
        return dataBase;
    }

    public void setDataBase(List<double[]> dataBase) {
        this.dataBase = dataBase;
    }

    public Position WeightedKNearestAlgorithm(double [] testPoint, int k){

        double eucDist, dist, x, y;
        double xsum = 0, ysum = 0;
        EucladianDistance eucEntry;
        eucDistances = new ArrayList<>();

        for (int i = 0; i < dataBase.size(); i++)
        {
            eucDist = 0;
            for (int j = 0; j < NUMBER_OF_ACCESS_POINTS; j++)
            {
                dist = Math.pow((double)(testPoint[j]-dataBase.get(i)[j+2]),2);
                eucDist += dist;
            }
            eucDist = Math.sqrt((double)eucDist);
            eucEntry = new EucladianDistance(i,eucDist);
            eucDistances.add(eucEntry);
            //System.out.println("EucDist:" + eucEntry.getEucDist() + " " + eucEntry.getDbIndex());
        }

        // Sorting
        Collections.sort(eucDistances, new Comparator<EucladianDistance>() {
            @Override
            public int compare(EucladianDistance eucDist2, EucladianDistance eucDist1)
            {
                return Double.compare(eucDist2.getEucDist(),eucDist1.getEucDist());
            }
        });

        for (int i = 0; i < k; i++)
        {
            xsum = xsum + dataBase.get(eucDistances.get(i).getDbIndex())[0];
            ysum = ysum + dataBase.get(eucDistances.get(i).getDbIndex())[1];
           // System.out.println(" eucDistances: " + eucDistances.get(i).getDbIndex()+ " " + eucDistances.get(i).getEucDist());
        }

        x = xsum/k;
        y = ysum/k;

        System.out.println("WKNN CALLED");
        Position estimatedPosition = new Position(x,y);
        return estimatedPosition;

    }

    public Position PhyWeightedKNearestAlgorithm(double [] testPoint, int k){

        double eucDist, refDist, dist, rdist, xcor, ycor, weight;
        double x = 0, y = 0;
        int numOfCols = 0;
        EucladianDistance eucEntry;
        eucDistances = new ArrayList<>();

        for (int i = 0; i < dataBase.size(); i++)
        {
            eucDist = 0;
            refDist = 0;
            numOfCols = 0;
            for (int j = 0; j < NUMBER_OF_ACCESS_POINTS; j++)
            {
                if(testPoint[j] != -1)
                {
                    numOfCols++;
                    rdist = Math.pow(calculateDistanceFromRSSI(dataBase.get(i)[j]),2);
                    dist = Math.pow(( calculateDistanceFromRSSI(testPoint[j]) - calculateDistanceFromRSSI(dataBase.get(i)[j])),2);
                    eucDist += dist;
                    refDist += rdist;
                }
            }
            eucDist = Math.sqrt((double)eucDist/numOfCols);
            refDist = Math.sqrt((double)refDist/numOfCols);
            eucEntry = new EucladianDistance(i,eucDist,refDist);
            eucDistances.add(eucEntry);
            System.out.println("EucDist:" + eucEntry.getDbIndex() + " " + eucEntry.getEucDist() + " " + eucEntry.getRefDist());
        }

        // Sorting
        Collections.sort(eucDistances, new Comparator<EucladianDistance>() {
            @Override
            public int compare(EucladianDistance eucDist2, EucladianDistance eucDist1)
            {
                return Double.compare(eucDist2.getEucDist(),eucDist1.getEucDist());
            }
        });

        // Assigning weights

        double sumDiffDist = 0, sumRefDist = 0, sumDDWxRPDW = 0;

        // Summing inverse of ref and riff distances
        for (int i = 0; i < k; i++)
        {
            sumDiffDist += 1/eucDistances.get(i).getEucDist();
            sumRefDist += 1/eucDistances.get(i).getRefDist();
        }

        // Assigning DDW and RPDW
        for (int i = 0; i < k; i++)
        {
            eucDistances.get(i).setDDW((1/eucDistances.get(i).getEucDist())/sumDiffDist);
            eucDistances.get(i).setRPDW((1/eucDistances.get(i).getRefDist())/sumRefDist);
        }

        // Summing DDW*RPDW
        for (int i = 0; i < k; i++)
        {
            sumDDWxRPDW += (eucDistances.get(i).getDDW()*eucDistances.get(i).getRPDW());
        }

        // Assigning Weights DDW*RPDW
        for (int i = 0; i < k; i++)
        {
            eucDistances.get(i).setWeight((eucDistances.get(i).getDDW()*eucDistances.get(i).getRPDW())/sumDDWxRPDW);
        }

        // Selecting desired indexed x and y coordinates from database
        for (int i = 0; i < k; i++)
        {
            xcor = dataBase.get(eucDistances.get(i).getDbIndex())[0];
            ycor = dataBase.get(eucDistances.get(i).getDbIndex())[1];
            weight = eucDistances.get(i).getWeight();
            //System.out.println("\n Weight " + i + " " + weight + " xcor, ycor: " + xcor + " " + ycor);
            x += xcor*weight;
            y += ycor*weight;
        }

        //x = Math.round((double)x*100)/100;
        //y = Math.round((double)y*100)/100;
        System.out.println("PhyWKNN Estimation");
        System.out.println("\n \n PHY Estimated x and y: " + x + " " + y + " \n\n");
        Position estimatedPosition = new Position(x,y,true);
        return estimatedPosition;

    }


    public double calculateDistanceFromRSSI(double rssi)
    {
        double txPower = TX_POWER;
        double ratio = rssi*1.0/txPower;
        double distance;
        if (ratio < 1.0)
        distance = Math.pow(ratio, 10);
        else
        distance = (0.89976)*Math.pow(ratio,7.7095)+0.111;
        return distance;
    }

}
