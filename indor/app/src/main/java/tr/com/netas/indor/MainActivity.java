package tr.com.netas.indor;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.androidnetworking.common.Priority;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import tr.com.netas.indor.Constants.Devices;
import tr.com.netas.indor.Models.RSSIEntry;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    BluetoothAdapter bluetoothAdapter;
    BluetoothManager bluetoothManager;
    BluetoothLeScanner bluetoothScanner;

    final static int REQUEST_ENABLE_BT = 1;

    RSSIEntry rssiEntry;

    TextView peripheralTextView;
    TextView AP1;
    TextView AP2;
    TextView AP3;
    TextView AP4;
    TextView AP5;
    TextView AP6;
    TextView totalPoint;

    Button startScanningButton;
    Button stopScanningButton;
    Button sendValuesButton;

    EditText xcorTextView;
    EditText ycorTextView;
    Chronometer chronometerView;

    private int deviceNo = 0;
    private int apCount1;
    private int apCount2;
    private int apCount3;
    private int apCount4;
    private int apCount5;
    private int apCount6;
    private float xcor, ycor;

    // Settings for Scan Filter
    ScanSettings settings = new ScanSettings.Builder().setScanMode(ScanSettings.CALLBACK_TYPE_ALL_MATCHES).build();

    ArrayList scanFilters;
    ArrayList<RSSIEntry> rssiEntries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AndroidNetworking.initialize(getApplicationContext());

        bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        bluetoothScanner = bluetoothAdapter.getBluetoothLeScanner();

        peripheralTextView = (TextView) findViewById(R.id.PeripheralTextView);
        peripheralTextView.setMovementMethod(new ScrollingMovementMethod());

        startScanningButton = (Button) findViewById(R.id.StartScanButton);
        startScanningButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startScanning();
            }
        });


        stopScanningButton = (Button) findViewById(R.id.StopScanButton);
        stopScanningButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                stopScanning();
            }
        });
        stopScanningButton.setVisibility(View.INVISIBLE);

        sendValuesButton = (Button) findViewById(R.id.sendValuesButton);
        sendValuesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendValues();
            }
        });

        sendValuesButton.setVisibility(View.INVISIBLE);
        chronometerView = (Chronometer) findViewById(R.id.chronometerView);

        AP1 = (TextView) findViewById(R.id.accessPoint1);
        AP2 = (TextView) findViewById(R.id.accessPoint2);
        AP3 = (TextView) findViewById(R.id.accessPoint3);
        AP4 = (TextView) findViewById(R.id.accessPoint4);
        AP5 = (TextView) findViewById(R.id.accessPoint5);
        AP6 = (TextView) findViewById(R.id.accessPoint6);
        totalPoint = (TextView) findViewById(R.id.totalPoint);

        // Ensures Bluetooth is available on the device and it is enabled. If not, displays a dialog requesting user permission to enable Bluetooth.
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BT);
        }

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
                Toast.makeText(this, "The permission to get BLE location data is required", Toast.LENGTH_SHORT).show();
            }else{
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }

        scanFilters = new ArrayList<>();

        //Adding the filtered device names to the filters list
        for (int i=0; i< Devices.FILTERED_DEVICES.length ; i++) {
            ScanFilter filter = new ScanFilter.Builder().setDeviceName(Devices.FILTERED_DEVICES[i]).build();
            scanFilters.add(filter);
        }

    }

    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {

            peripheralTextView.append(result.getDevice().getName() + " RSSI: " + result.getRssi() + "\n");

            if(result.getDevice().getName().equals(Devices.FILTERED_DEVICES[0]))
            {
                apCount1++;
                AP1.setText(Integer.toString(apCount1));
                deviceNo = Devices.DEVICE1;
            }
            else if(result.getDevice().getName().equals(Devices.FILTERED_DEVICES[1]))
            {
                apCount2++;
                AP2.setText(Integer.toString(apCount2));
                deviceNo = Devices.DEVICE2;
            }
            else if(result.getDevice().getName().equals(Devices.FILTERED_DEVICES[2]))
            {
                apCount3++;
                AP3.setText(Integer.toString(apCount3));
                deviceNo = Devices.DEVICE3;
            }
            else if(result.getDevice().getName().equals(Devices.FILTERED_DEVICES[3]))
            {
                apCount4++;
                AP4.setText(Integer.toString(apCount4));
                deviceNo = Devices.DEVICE4;
            }
            else if(result.getDevice().getName().equals(Devices.FILTERED_DEVICES[4]))
            {
                apCount5++;
                AP5.setText(Integer.toString(apCount5));
                deviceNo = Devices.DEVICE5;
            }
            else if(result.getDevice().getName().equals(Devices.FILTERED_DEVICES[5]))
            {
                apCount6++;
                AP6.setText(Integer.toString(apCount6));
                deviceNo = Devices.DEVICE6;
            }

            totalPoint.setText(Integer.toString(apCount1+apCount2+apCount3+apCount4+apCount5+apCount6));

            rssiEntry = new RSSIEntry();
            rssiEntry.setRssi(result.getRssi());
            rssiEntry.setXcor(xcor);
            rssiEntry.setYcor(ycor);
            rssiEntry.setDevice(deviceNo);
            rssiEntries.add(rssiEntry);

            // Auto scroll for text view
            final int scrollAmount = peripheralTextView.getLayout().getLineTop(peripheralTextView.getLineCount()) - peripheralTextView.getHeight();
            // If there is no need to scroll, scrollAmount will be <=0
            if (scrollAmount > 0)
                peripheralTextView.scrollTo(0, scrollAmount);
        }
    };


    public void startScanning() {

        System.out.println("Start scanning");

        apCount1 = 0;
        apCount2 = 0;
        apCount3 = 0;
        apCount4 = 0;
        apCount5 = 0;
        apCount6 = 0;

        AP1.setText("0");
        AP2.setText("0");
        AP3.setText("0");
        AP4.setText("0");
        AP5.setText("0");
        AP6.setText("0");
        totalPoint.setText("0");

        chronometerView.setBase(SystemClock.elapsedRealtime());
        chronometerView.start();

        sendValuesButton.setVisibility(View.INVISIBLE);

         xcorTextView = (EditText) findViewById(R.id.xcoordinate);
         ycorTextView = (EditText) findViewById(R.id.ycoordinate);

        xcor = Float.parseFloat(xcorTextView.getText().toString());
        ycor = Float.parseFloat(ycorTextView.getText().toString());

        xcorTextView.setFocusable(false);
        ycorTextView.setFocusable(false);

        rssiEntries = null;
        rssiEntry = null;
        rssiEntries = new ArrayList<RSSIEntry>();

        peripheralTextView.setText("");
        startScanningButton.setVisibility(View.INVISIBLE);
        stopScanningButton.setVisibility(View.VISIBLE);

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("Scanning is started.");
                bluetoothScanner.startScan(scanFilters, settings, leScanCallback);
               // bluetoothScanner.startScan(leScanCallback);
            }
        });
    }

    public void sendValues() {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("SENDING TO SERVER")
                    .setMessage("Are you sure to send the RSSI values to the server?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if(rssiEntries != null) {

                                JSONObject jObject = new JSONObject();
                                try
                                {
                                    JSONArray jArray = new JSONArray();
                                    for (int i = 0; i < rssiEntries.size();i++)
                                    {
                                        JSONObject entryJSON = new JSONObject();
                                        entryJSON.put("rssi", rssiEntries.get(i).getRssi());
                                        entryJSON.put("xcor", rssiEntries.get(i).getXcor());
                                        entryJSON.put("ycor", rssiEntries.get(i).getYcor());
                                        entryJSON.put("device", rssiEntries.get(i).getDevice());
                                        System.out.println(rssiEntries.get(i).getDevice());
                                        jArray.put(entryJSON);
                                    }
                                    jObject.put("RSSIValues", jArray);
                                } catch (JSONException jse) {
                                    jse.printStackTrace();
                                }

                                // System.out.println("selam: "+jObject);

                                AndroidNetworking.post("http://192.168.145.1:3000/send_rssi")
                                        .addJSONObjectBody(jObject) // posting json
                                        .setTag("test")
                                        .setPriority(Priority.HIGH)
                                        .build()
                                        .getAsJSONObject(new JSONObjectRequestListener() {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                System.out.println(response);
                                                Toast.makeText(MainActivity.this, "Values has been sent.", Toast.LENGTH_SHORT).show();
                                                System.out.println("We have a respond!");
                                            }
                                            @Override
                                            public void onError(ANError error) {
                                                System.out.println(error);
                                                System.out.println("Error occured while networking.");
                                            }
                                        });
                            }
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();


    }

    public void stopScanning() {
        sendValuesButton.setVisibility(View.VISIBLE);
        chronometerView.stop();
        xcorTextView.setFocusableInTouchMode(true);
        ycorTextView.setFocusableInTouchMode(true);


        System.out.println("stopping scanning");
        peripheralTextView.append("Stopped Scanning");
        startScanningButton.setVisibility(View.VISIBLE);
        stopScanningButton.setVisibility(View.INVISIBLE);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                bluetoothScanner.stopScan(leScanCallback);
            }
        });
    }

}
