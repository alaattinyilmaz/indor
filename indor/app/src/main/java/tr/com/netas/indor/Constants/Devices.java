package tr.com.netas.indor.Constants;

public class Devices {

    public static final String[] FILTERED_DEVICES = {
            "MU-98072D9FF968",
            "MU-98072DA00215",
            "MU-98072DA570CC",
            "MU-98072DA5A5AD",
            "MU-98072D9FFBDD",
            "MU-98072DA57505"
    };

    public static final String[] FILTERED_DEVICES2 = {
            "MU-98072D9FFF78",
            "MU-98072DA5723F",
            "MU-98072DA00262",
            "MU-98072D9FFE84"
    };

        public static final int DEVICE1 = 1;
        public static final int DEVICE2 = 2;
        public static final int DEVICE3 = 3;
        public static final int DEVICE4 = 4;
        public static final int DEVICE5 = 5;
        public static final int DEVICE6 = 6;
}
