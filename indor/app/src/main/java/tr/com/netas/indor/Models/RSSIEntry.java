package tr.com.netas.indor.Models;

public class RSSIEntry {
    private int rssi;
    private float xcor;
    private float ycor;
    private int device;

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public float getXcor() {
        return xcor;
    }

    public void setXcor(float xcor) {
        this.xcor = xcor;
    }

    public float getYcor() {
        return ycor;
    }

    public void setYcor(float ycor) {
        this.ycor = ycor;
    }

    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }
}
